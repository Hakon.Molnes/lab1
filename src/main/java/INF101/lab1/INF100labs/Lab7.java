package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        /* 
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }
        // [21, 22, 23]
        // [31, 32, 33]

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }
        // [11, 12, 13]
        // [31, 32, 33]
        */


        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grid1);
        System.out.println(equalSums1); // false


        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grid2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grid2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grid2);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grid3);
        System.out.println(equalSums3); // true

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        //Deleting choosen row, tbh too much googling to find this simple function existed...
        grid.remove(row);
    }




    // Function to calculate the sum of integers in a list
    private static int calculateSum(ArrayList<Integer> list) {
        int sum = 0;
        for (int num : list) {
            sum += num;
        }
        return sum;
    }

      // Function to get a specific column from a matrix
      private static ArrayList<Integer> getColumn(ArrayList<ArrayList<Integer>> matrix, int col) {
        ArrayList<Integer> column = new ArrayList<>();
        for (ArrayList<Integer> row : matrix) {
            column.add(row.get(col));
        }
        return column;
    }

    private static boolean checkequality(ArrayList<Integer> list){
        //first element as the reference 
        int referenceValue = list.get(0);

        // check all elements 
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) != referenceValue) {
                return false; //element different from the reference value
            }
        }

        return true;
    }
    

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
         // Calculate sum of rows and storing each value
         ArrayList<Integer> RValues = new ArrayList<>();
         for (ArrayList<Integer> row : grid) {
            int rowSum = calculateSum(row);
            RValues.add(rowSum);
            //System.out.println("Sum of row: " + rowSum);
        }

        // Calculate sum of columns and storing each value
        ArrayList<Integer> CValues = new ArrayList<>();
        for (int col = 0; col < grid.get(0).size(); col++) {
            ArrayList<Integer> column = getColumn(grid, col);
            int colSum = calculateSum(column);
            CValues.add(colSum);
            //System.out.println("Sum of column " + col + ": " + colSum);
        }

        Boolean s = checkequality(CValues);
        Boolean t = checkequality(RValues);

        Boolean result = s && t;

        return result;
     
    }

}