package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("pear", "apple", "ananas");
        findLongestWords("Game", "Action", "Champion");
        findLongestWords("four", "five", "nine");

        boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); // false

        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); // true

        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); // false

        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4); // true

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false


        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
    
        String[] list = {word1, word2, word3};
        int wordcheck = 0;
        List<String> BigW = new ArrayList<>();  
        
        for (int i = 0; i < list.length; i++){
            
            int x = list[i].length();
            if (x > wordcheck){

                //Registers the length of the word and baptizes it
                wordcheck = list[i].length();
                BigW.clear();
                BigW.add(list[i]);

            } else if (x == wordcheck){
                BigW.add(list[i]);
            }

        }

        for (String words : BigW){
            System.out.println(words);

        }


    }

    public static boolean isLeapYear(int year) {
        if (year % 4 != 0){
            return false;

        }
        if (year % 100 == 0 && year % 400 != 0) {
            return false;
        }
        else{
            return true;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 !=0){
            return false;
        }

        if (num < 0){
            return false;
        }

        else{
            return true;
        }
    }

}
