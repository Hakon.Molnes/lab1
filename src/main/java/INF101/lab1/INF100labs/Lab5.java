package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        /* 
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> multipliedList1 = multipliedWithTwo(list1);
        System.out.println(multipliedList1); // [2, 4, 6, 8]

        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(2, 2));
        ArrayList<Integer> multipliedList2 = multipliedWithTwo(list2);
        System.out.println(multipliedList2); // [4, 4]

        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(0));
        ArrayList<Integer> multipliedList3 = multipliedWithTwo(list3);
        System.out.println(multipliedList3); // [0]

        

        ArrayList<Integer> list_1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> removedList1 = removeThrees(list_1);
        System.out.println(removedList1); // [1, 2, 4]

        ArrayList<Integer> list_2 = new ArrayList<>(Arrays.asList(1, 2, 3, 3));
        ArrayList<Integer> removedList2 = removeThrees(list_2);
        System.out.println(removedList2); // [1, 2]

        ArrayList<Integer> list_3 = new ArrayList<>(Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3));
        ArrayList<Integer> removedList3 = removeThrees(list_3);
        System.out.println(removedList3); // [1, 2, 4]

        ArrayList<Integer> list_4 = new ArrayList<>(Arrays.asList(3, 3));
        ArrayList<Integer> removedList4 = removeThrees(list_4);
        System.out.println(removedList4); // []

        
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> removedList1 = uniqueValues(list1);
        System.out.println(removedList1); // [1, 2, 3]

         
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 5));
        ArrayList<Integer> removedList2 = uniqueValues(list2);
        System.out.println(removedList2); // [4, 5]
        */

        ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b1 = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a1, b1);
        System.out.println(a1); // [5, 4, 0]

        ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2); // [48, 23, -27]

        

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> Dlist = new ArrayList<Integer>(); 
        //Loops through all numbers from the argument-list
        for (int number : list) {
            int n2 = number*2;
            Dlist.add(n2);
        }
        return Dlist;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> Zlist = new ArrayList<Integer>(); 
        
        for (int num : list){
            if (num != 3){
                Zlist.add(num);
            }
    
        }
        return Zlist;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
       //HashSet to keep track of unique values
        HashSet<Integer> uniqueSet = new HashSet<>(list);

        //new list with unique values
        ArrayList<Integer> uniqueList = new ArrayList<>(uniqueSet);

        return uniqueList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        //Iterates through each list-index, then modifies list a by accessing values from b
        for (int i = 0; i < a.size() && i < b.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
        
        
    }

}