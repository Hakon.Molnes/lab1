package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        multiplesOfSevenUpTo(49);
        multiplicationTable(5);
        
        int sum = crossSum(1);
        System.out.println(sum); // 1

        int sum1 = crossSum(12);
        System.out.println(sum1); // 3

        int sum2 = crossSum(123);
        System.out.println(sum2); // 6

        int sum3 = crossSum(1234);
        System.out.println(sum3); // 10

        int sum4 = crossSum(4321);
        System.out.println(sum4); // 10 

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 7; i <= n; i+= 7){
            System.out.println(i);
        }

    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                int result = i * j;
                System.out.print(result + "\t");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        //Finding how many digits are in the number
        int counter = 0;
        int holder = num;
        while (holder != 0){
            holder /= 10;
            counter ++;

        }

        holder = num;
        
        //Taking each digit and adding to a list
        List<Integer> Diglist = new ArrayList<>();  
        for (int i = counter - 1; i >= 0; i--) {
            int digit = holder / (int) Math.pow(10, i);
            holder %= Math.pow(10, i);
            Diglist.add(digit);
        }

        //Summing up the digits from list to get crossSum
        int sum = 0;

        // Iterate through the elements and add them up
        for (int number : Diglist) {
            sum += number;
        }

        return sum;

    }

}